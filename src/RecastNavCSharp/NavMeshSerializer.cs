﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Detour;
using System.Linq;

[Serializable]
public class NavMeshSerializer
{
    public NavMeshParams Param { get; set; }
    public NavMeshBuilder[] NavMeshBuilders { get; set; }

    public NavMeshSerializer()
    {
    }

    public NavMeshSerializer(NavMesh navMesh) : this()
    {
        Decompose(navMesh);
    }

    public void Decompose(NavMesh navMesh)
    {
        Param = navMesh.Param;

        NavMeshBuilders = new NavMeshBuilder[navMesh._tiles.Length];
        for (int i = 0; i < navMesh._tiles.Length; i++)
        {
            if (navMesh._tiles[i].Data != null)
            {
                NavMeshBuilders[i] = navMesh._tiles[i].Data;
            }
        }
    }

    public NavMesh Reconstitute()
    {
        NavMesh navMesh = new NavMesh();
        navMesh.Init(Param);

        var tempRef = 0u;
        var temp = 0u;
        
        for (int i = 0; i < NavMeshBuilders.Length; i++)
        {
            if (NavMeshBuilders[i] != null)
            {
                try
                {
                    navMesh.AddTile(NavMeshBuilders[i], NavMesh.TileFreeData, tempRef, ref temp);
                }
                catch (Exception e)
                {
                    Exception asdf = e;
                    throw asdf;
                }
                
                //tempRef = temp;
            }
        }
        return navMesh;
    }

    public static NavMesh DeserializeAggregateBinary(Stream stream)
    {
        var navMesh = new NavMesh();
        navMesh.Init(DeserializeBinaryParams(stream));

        var tempRef = 0u;
        var temp = 0u;
        while (stream.Position < stream.Length)
        {
            //todo: are we storing all the tiles in one file?
            var pos = stream.Position;
            int dataSize = -1;
            NavMeshBuilder builder = null;
            try
            {
                builder = DeserializeBinaryTile(stream, out dataSize);
            }
            catch (Exception e)
            {
                if (dataSize == -1) //we didn't get the size out of it
                    throw new Exception("Tile deserialization failed at " + pos, e);

                //we're fine, we got the size out, so we can seek to the next tile.
                stream.Seek(pos + dataSize, SeekOrigin.Begin);
            }

            if (builder == null) continue;

            try
            {
                navMesh.AddTile(builder, NavMesh.TileFreeData, tempRef, ref temp);
            }
            catch (Exception e)
            {
                var asdf = e;
                throw asdf;
            }
        }

        return navMesh;
    }

    public static unsafe NavMeshParams DeserializeBinaryParams(Stream stream)
    {
        var bytes = ReadBytes(stream, dtAlign4(sizeof (dtNavMeshParams)));
        fixed (byte* bp = bytes)
        {
            var dp = (dtNavMeshParams*) bp;

            return new NavMeshParams
            {
                MaxPolys = dp->MaxPolys,
                MaxTiles = dp->MaxTiles,
                Orig = CopyPrim<float>(dp->Orig, sizeof(float), dtNavMeshParams.OrigSize),
                TileHeight = dp->TileHeight,
                TileWidth = dp->TileWidth
            };
        }
    }

    public static unsafe NavMeshBuilder DeserializeBinaryTile(Stream stream, out int dataSize)
    {
        const int DT_NAVMESH_MAGIC = 'D' << 24 | 'N' << 16 | 'A' << 8 | 'V';
        const int DT_NAVMESH_VERSION = 7;
        const int DT_NAVMESH_STATE_MAGIC = 'D' << 24 | 'N' << 16 | 'M' << 8 | 'S';
        const int DT_NAVMESH_STATE_VERSION = 1;

        var builder = new NavMeshBuilder();

        //because dtCreateNavMeshData sticks the array size in the beginning
        var bytes = ReadBytes(stream, 4);
        dataSize = BitConverter.ToInt32(bytes, 0);

        //order of the 'format' recast serializes to:
        //header
        //verts
        //polys
        //links
        //detailpolys
        //detailverts
        //detailtris
        //bvtrees
        //offmeshcons

        #region header
        bytes = ReadBytes(stream, dtAlign4(sizeof(dtMeshHeader)));
        var header = new MeshHeader();
        fixed (byte* bp = bytes)
        {
            var h = (dtMeshHeader*)bp;

            if (h->Magic != DT_NAVMESH_MAGIC)
                throw new Exception("Unsupported binary navmesh magic " + header.Magic);
            if (h->Version != DT_NAVMESH_VERSION)
                throw new Exception("Unsupported binary navmesh version " + header.Version);

            header.Magic = h->Magic;
            header.Version = h->Version;
            header.X = h->X;
            header.Y = h->Y;
            header.Layer = h->Layer;
            header.UserId = h->UserId;
            header.PolyCount = h->PolyCount;
            header.VertCount = h->VertCount;
            header.MaxLinkCount = h->MaxLinkCount;

            header.DetailMeshCount = h->DetailMeshCount;

            header.DetailVertCount = h->DetailVertCount;

            header.BVNodeCount = h->BVNodeCount;
            header.OffMeshConCount = h->OffMeshConCount;
            header.OffMeshBase = h->OffMeshBase;
            header.WalkableHeight = h->WalkableHeight;
            header.WalkableRadius = h->WalkableRadius;
            header.WalkableClimb = h->WalkableClimb;
            header.BMin = CopyPrim<float>(h->BMin, sizeof(float), dtMeshHeader.BMinSize);
            header.BMax = CopyPrim<float>(h->BMax, sizeof(float), dtMeshHeader.BMaxSize);
        }
        #endregion

        #region vertices
        var verts = new float[header.VertCount * 3];
        bytes = ReadBytes(stream, dtAlign4(sizeof(float) * verts.Length));
        fixed (byte* bp = bytes)
        fixed(float* vp = verts)
        {
            var navVerts = (float*)bp;
            var vc = verts.Length;

            for (var i = 0; i < vc; i++)
            {
                vp[i] = navVerts[i];
            }
        }
        #endregion
        
        #region polys
        var polys = new Poly[header.PolyCount];
        bytes = ReadBytes(stream, dtAlign4(sizeof(dtPoly) * polys.Length));
        fixed (byte* bp = bytes)
        {
            var pp = (dtPoly*) bp;

            for (var i = 0; i < polys.Length; i++)
            {
                var p = &pp[i];
                var poly = new Poly
                {
                    FirstLink = p->FirstLink,
                    Flags = p->Flags,
                    _areaAndType = p->_areaAndType,
                    VertCount = p->VertCount
                };

                for (var j = 0; j < dtPoly.DT_VERTS_PER_POLYGON; j++)
                {
                    poly.Verts[j] = checked((ushort)p->Verts[j]);
                    poly.Neis[j] = checked((ushort)p->Neis[j]);
                }

                polys[i] = poly;
            }
        }
        #endregion

        #region links
        var links = new Link[header.MaxLinkCount];
        bytes = ReadBytes(stream, dtAlign4(sizeof(dtLink) * links.Length));
        fixed (byte* bp = bytes)
        {
            var dp = (dtLink*) bp;

            for (var i = 0; i < links.Length; i++)
            {
                var p = &dp[i];
                var v = new Link
                {
                    BMax = p->bmax,
                    BMin = p->bmin,
                    Edge = p->edge,
                    Next = p->next,
                    Ref = p->@ref,
                    Side = p->side
                };

                links[i] = v;
            }
        }
        #endregion

        #region detailpolys
        var detailPolys = new PolyDetail[header.PolyCount];
        bytes = ReadBytes(stream, dtAlign4(sizeof(dtPolyDetail) * detailPolys.Length));
        fixed (byte* bp = bytes)
        {
            var pp = (dtPolyDetail*) bp;

            for (var i = 0; i < detailPolys.Length; i++)
            {
                var p = &pp[i];
                var poly = new PolyDetail
                {
                    TriBase = p->TriBase,
                    TriCount = p->TriCount,
                    VertBase = p->VertBase,
                    VertCount = p->VertCount
                };

                detailPolys[i] = poly;
            }
        }
        #endregion

        #region detailverts
        var detailVerts = new float[header.DetailVertCount];
        bytes = ReadBytes(stream, dtAlign4(sizeof(float) * detailVerts.Length));
        fixed(byte* bp = bytes)
        fixed (float* vp = detailVerts)
        {
            var v = (float*) bp;
            var vc = detailVerts.Length;

            for (var i = 0; i < vc; i++)
            {
                vp[i] = v[i];
            }
        }
        #endregion

        #region detailtris
        var detailTris = new short[header.DetailTriCount];
        bytes = ReadBytes(stream, dtAlign4(sizeof(byte) * detailTris.Length));
        fixed (byte* bp = bytes)
        {
            //triangle details are saved as 'unsigned char', which is a byte.
            for (var i = 0; i < detailTris.Length; i++)
            {
                detailTris[i] = bp[i];
            }
        }
        #endregion

        #region bvtrees
        var bvTrees = new BVNode[header.BVNodeCount];
        bytes = ReadBytes(stream, dtAlign4(sizeof(dtBVNode) * bvTrees.Length));
        fixed (byte* bp = bytes)
        {
            var dp = (dtBVNode*) bp;

            for (var i = 0; i < bvTrees.Length; i++)
            {
                var p = &dp[i];
                var v = new BVNode(CopyPrim<int>(p->BMin, sizeof(int), dtBVNode.BMinSize), CopyPrim<int>(p->BMax, sizeof(int), dtBVNode.BMaxSize), p->I);

                bvTrees[i] = v;
            }
        }
        #endregion

        #region offmeshcons
        var offMeshCons = new OffMeshConnection[header.OffMeshConCount];
        bytes = ReadBytes(stream, dtAlign4(sizeof(dtOffMeshConnection) * offMeshCons.Length));
        fixed (byte* bp = bytes)
        {
            var dp = (dtOffMeshConnection*) bp;

            for (var i = 0; i < offMeshCons.Length; i++)
            {
                var p = &dp[i];
                var v = new OffMeshConnection
                {
                    Pos = CopyPrim<float>(p->Pos, sizeof (float), dtOffMeshConnection.PosSize),
                    Poly = p->Poly,
                    Side = p->Side,
                    Flags = p->Flags,
                    Rad = p->Rad,
                    UserId = p->UserId
                };

                offMeshCons[i] = v;
            }
        }
        #endregion

        builder.Header = header;
        builder.NavVerts = verts;
        builder.NavPolys = polys;
        builder.NavLinks = links;
        builder.NavDMeshes = detailPolys;
        builder.NavDVerts = detailVerts;
        builder.NavDTris = detailTris;
        builder.NavBvTree = bvTrees;
        builder.OffMeshCons = offMeshCons;

        return builder;
    }

    static int dtAlign4(int x) { return (x + 3) & ~3; }

    private static byte[] ReadBytes(Stream stream, int size)
    {
        //http://www.yoda.arachsys.com/csharp/readbinary.html
        var buffer = new byte[size];
        var offset = 0;
        var remaining = buffer.Length;
        while (remaining > 0)
        {
            int read = stream.Read(buffer, offset, remaining);
            if (read <= 0)
                throw new EndOfStreamException
                    (String.Format("End of stream reached with {0} bytes left to read", remaining));
            remaining -= read;
            offset += read;
        }
        return buffer;
    }

    private static unsafe T[] CopyPrim<T>(void* source, int tSize, int size)
        where T : struct
    {
        var arr = new T[size];
        var bsource = (byte*) source;

        var handle = GCHandle.Alloc(arr, GCHandleType.Pinned);
        var dest = (byte*) handle.AddrOfPinnedObject().ToPointer();
        var blength = size*tSize;
        for (var i = 0; i < blength; i++)
            dest[i] = bsource[i];
        handle.Free();

        return arr;
    }
}
