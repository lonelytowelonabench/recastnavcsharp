﻿using System;
using System.Numerics;
using Detour;
using Recast.Data;
using static Recast.Data.Helper;
using dtPolyRef = System.UInt32;

namespace RecastNavCSharp.Crowd
{
    public class CrowdAgent
    {
        public static int CrowdAgentMaxNeighbors = 6;
        public static int CrowdAgentMaxCorners = 4;

        public bool Active;
        public CrowdAgentState State;

        public PathCorridor Corridor;
        public LocalBoundary Boundary;

        public float TopologyOptTime;

        public CrowdNeighbor[] Neis = new CrowdNeighbor[CrowdAgentMaxNeighbors];
        public int NNeis;
        public float DesiredSpeed;

        public float[] npos = new float[3];
        public float[] disp = new float[3];
        public float[] dvel = new float[3];
        public float[] nvel = new float[3];
        public float[] vel = new float[3];

        public CrowdAgentParams Param;
        public float[] CornerVerts = new float[CrowdAgentMaxCorners*3];
        public short[] CornerFlags = new short[CrowdAgentMaxCorners];
        public dtPolyRef[] CornerPolys = new dtPolyRef[CrowdAgentMaxCorners];

        public int NCorners;
        public MoveRequestState TargetState;
        public dtPolyRef TargetRef;
        public float[] TargetPos = new float[3];
        public long TargetPathQRef;
        public bool TargetReplan;
        public float TargetReplanTime;

        public CrowdAgent()
        {
            Corridor = new PathCorridor();
            Boundary = new LocalBoundary();
            for (int i = 0; i < CrowdAgentMaxNeighbors; i++)
            {
                Neis[i] = new CrowdNeighbor();
            }
        }

        public void Integrate(float dt)
        {
            float maxDelta = Param.MaxAcceleration*dt;
            var dv = V3(nvel) - V3(vel);

            float ds = dv.Length();
            if (ds > maxDelta)
                dv = dv * (maxDelta / ds);

            vel = Helper.F3(Helper.V3(vel) + dv);

            if(Helper.VLen(vel) > 0.0001f)
                Helper.VMad(ref npos, npos, vel, dt);
            else
            {
                Helper.VSet(ref vel, 0, 0, 0);
            }
        }

        public bool OverOffMeshConnection(float radius)
        {
            if (NCorners <= 0)
                return false;

            bool offMeshConnection = (CornerFlags[NCorners - 1] & NavMeshQuery.StraightPathOffMeshConnection) != 0;
            if (offMeshConnection)
            {
                float distSq = Helper.VDist2DSqr(npos[0], npos[1], npos[2], CornerVerts[(NCorners - 1)*3 + 0], CornerVerts[(NCorners - 1)*3 + 1], CornerVerts[(NCorners - 1)*3 + 2]);
                if (distSq < radius*radius)
                    return true;
            }
            return false;
        }

        public float GetDistanceToGoal(float range)
        {
            if (NCorners <= 0)
                return range;

            bool endOfPath = (CornerFlags[NCorners - 1] & NavMeshQuery.StraightPathEnd) != 0;
            if (endOfPath)
            {
                float[] temp = new float[3];
                Array.Copy(CornerVerts, (NCorners-1)*3, temp, 0, 3);
                return Math.Min(Helper.VDist2D(npos, temp), range);
            }

            return range;
        }

        public void CalcSmoothSteerDirection(ref Vector3 dir)
        {
            if (NCorners <= 0)
            {
                dir = new Vector3();
                return;
            }

            int ip0 = 0;
            int ip1 = Math.Min(1, NCorners - 1);
            var p0 = new Vector3();
            var p1 = new Vector3();

            Helper.CopyTo(CornerVerts, ip0*3, ref p0);
            Helper.CopyTo(CornerVerts, ip1*3, ref p1);

            var dir0 = p0 - Helper.V3(npos);
            var dir1 = p1 - Helper.V3(npos);

            dir0.Y = 0;
            dir1.Y = 0;

            float len0 = dir0.Length();
            float len1 = dir1.Length();

            if (len1 > 0.001f)
                dir1 = dir1 * (1.0f/len1);

            dir = dir0 - dir1*len0*0.5f;
            dir.Y = 0;

            dir = Vector3.Normalize(dir);
        }

        public void CalcStraightSteerDirection(ref Vector3 dir)
        {
            if (NCorners <= 0)
            {
                dir = new Vector3();
                return;
            }

            dir = V3(CornerVerts) - V3(npos);
            dir.Y = 0;
            dir = Vector3.Normalize(dir);
        }
    }

}