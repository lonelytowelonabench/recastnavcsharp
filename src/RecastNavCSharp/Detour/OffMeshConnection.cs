﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Detour
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    unsafe struct dtOffMeshConnection
    {
        public const int PosSize = 6;

        public fixed float Pos[PosSize];
        public float Rad;
        public ushort Poly;
        public byte Flags;
        public byte Side;
        public uint UserId;
    }

    [Serializable]
    public class OffMeshConnection
    {
        public float[] Pos { get; set; }
        public float Rad { get; set; }
        public int Poly { get; set; }
        public short Flags { get; set; }
        public short Side { get; set; }
        public long UserId { get; set; }

        public OffMeshConnection()
        {
            Pos = new float[6];
        }
    }
}
