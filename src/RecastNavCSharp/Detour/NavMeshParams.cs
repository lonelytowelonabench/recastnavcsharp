﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Detour
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    unsafe struct dtNavMeshParams
    {
        public const int OrigSize = 3;

        public fixed float Orig[OrigSize];
        public float TileWidth;
        public float TileHeight;
        public int MaxTiles;
        public int MaxPolys;
    }

    [Serializable]
    public class NavMeshParams
    {
        public float[] Orig { get; set; }
        public float TileWidth { get; set; }
        public float TileHeight { get; set; }
        public int MaxTiles { get; set; }
        public int MaxPolys { get; set; }

        public NavMeshParams()
        {
            Orig = new float[3];
        }
    }
}
