﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Detour
{
    internal class Vector3iArrayConverter : JsonConverter<Vector3i>
    {
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert == typeof(Vector3i);
        }

        public override Vector3i Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartArray)
                throw new JsonException();
            reader.Read();
            var x = reader.GetInt32();
            reader.Read();
            var y = reader.GetInt32();
            reader.Read();
            var z = reader.GetInt32();

            var v = new Vector3i(x, y, z);

            //finish the array.
            reader.Read();
            return v;
        }

        public override void Write(Utf8JsonWriter writer, Vector3i value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}