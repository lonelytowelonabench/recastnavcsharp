﻿using System.Collections.Generic;

namespace Detour.Compare
{
    public class CompareItemY : IComparer<BVNode>
    {
        int IComparer<BVNode>.Compare(BVNode a, BVNode b)
        {
            if (a != null && b != null)
            {

                if (a.BMin.Y < b.BMin.Y)
                    return -1;
                if (a.BMin.Y > b.BMin.Y)
                    return 1;
            }
            return 0;
        }
    }
}
