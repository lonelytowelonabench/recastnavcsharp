﻿using System.Collections.Generic;

namespace Detour.Compare
{
    public class CompareItemX : IComparer<BVNode>
    {
        public int Compare(BVNode a, BVNode b)
        {
            if (a != null && b != null)
            {
                if (a.BMin.X < b.BMin.X)
                    return -1;
                if (a.BMin.X > b.BMax.X)
                    return 1;
            }
            return 0;
        }
    }
}
