﻿using System.Collections.Generic;

namespace Detour.Compare
{
    public class CompareItemZ : IComparer<BVNode>
    {
        int IComparer<BVNode>.Compare(BVNode a, BVNode b)
        {
            if (a != null && b != null)
            {

                if (a.BMin.Z < b.BMin.Z)
                    return -1;
                if (a.BMin.Z > b.BMin.Z)
                    return 1;
            }
            return 0;
        }
    }
}
