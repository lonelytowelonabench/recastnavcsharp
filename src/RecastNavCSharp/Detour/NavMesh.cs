﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Recast.Data;
using static Recast.Data.Helper;

using dtPolyRef = System.UInt32;
using dtTileRef = System.UInt32;

namespace Detour
{
    [Serializable]
    public class NavMesh
    {
        public NavMeshParams _param;
        public float[] _orig = new float[3];
        public float _tileWidth;
        public float _tileHeight;
        public int _maxTiles;
        public int _tileLutSize;
        public int _tileLutMask;
        public MeshTile[] _posLookup;
        public MeshTile _nextFree;
        public MeshTile[] _tiles;

#if DT_POLYREF64
#else 
        uint _saltBits;
        uint _tileBits;
        uint _polyBits;
#endif

        public const uint NullLink = 0xffffffff;
        public const int TileFreeData = 1;
        public NavMeshParams Param
        {
            get { return _param; }
        }

        public NavMesh()
        {
            _orig = new float[3];
        }

        public Status Init(NavMeshParams param)
        {
            _param = param;
            Array.Copy(param.Orig, _orig, 3);
            _tileWidth = param.TileWidth;
            _tileHeight = param.TileHeight;

            _maxTiles = param.MaxTiles;
            _tileLutSize = (int)Helper.NextPow2((uint)(param.MaxTiles/4));
            if (_tileLutSize <= 0) _tileLutSize = 1;
            _tileLutMask = _tileLutSize - 1;

            _tiles = new MeshTile[_maxTiles];
            _posLookup = new MeshTile[_tileLutSize];

            for (int i = 0; i < _tileLutSize; i++)
            {
                _posLookup[i] = new MeshTile();
            }

            _nextFree = null;
            for (int i = _maxTiles-1; i >= 0; i--)
            {
                _tiles[i] = new MeshTile {Salt = 1, Next = _nextFree};
                _nextFree = _tiles[i];
            }

            _tileBits = Helper.Ilog2(Helper.NextPow2((uint)param.MaxTiles));
            _polyBits = Helper.Ilog2(Helper.NextPow2((uint)param.MaxPolys));
            _saltBits = Math.Min(31, 32 - _tileBits - _polyBits);
            if (_saltBits < 10)
            {
                return Status.Failure | Status.InvalidParam;
            }
            return Status.Success;
        }

        public Status Init(NavMeshBuilder data, int flags)
        {
            if(data.Header.Magic != Helper.NavMeshMagic)
                throw new ArgumentException("Wrong Magic Number");
            if (data.Header.Version != Helper.NavMeshVersion)
                throw new ArgumentException("Wrong Version Number");

            NavMeshParams param = new NavMeshParams();
            Array.Copy(data.Header.BMin, param.Orig, 3);
            param.TileWidth = data.Header.BMax[0] - data.Header.BMin[0];
            param.TileHeight = data.Header.BMax[2] - data.Header.BMin[2];
            param.MaxTiles = 1;
            param.MaxPolys = data.Header.PolyCount;

            Status status = Init(param);
            if ((status & Status.Failure) == Status.Failure)
                return status;

            dtTileRef temp = 0;
            return AddTile(data, flags, 0, ref temp);
        }

        public Status AddTile(NavMeshBuilder data, int flags, dtTileRef lastRef, ref dtTileRef result)
        {
            MeshHeader header = data.Header;
            if(header.Magic != Helper.NavMeshMagic)
                return Status.Failure | Status.WrongMagic;
            if (header.Version != Helper.NavMeshVersion)
                return Status.Failure | Status.WrongVersion;

            if(GetTileAt(header.X, header.Y, header.Layer) != null)
                return Status.Failure;

            MeshTile tile = null;
            if (lastRef == 0)
            {
                if (_nextFree != null)
                {
                    tile = _nextFree;
                    _nextFree = tile.Next;
                    tile.Next = null;
                }
            }
            else
            {
                int tileIndex = (int) DecodePolyIdTile((dtPolyRef)lastRef);
                if(tileIndex >= _maxTiles)
                    return Status.Failure | Status.OutOfMemory;

                MeshTile target = _tiles[tileIndex];
                MeshTile prev = null;
                tile = _nextFree;
                while (tile != null && tile != target)
                {
                    prev = tile;
                    tile = tile.Next;
                }

                if (tile != target)
                    return Status.Failure | Status.OutOfMemory;

                if (prev != null)
                    _nextFree = tile.Next;
                else
                {
                    prev.Next = tile.Next;
                }

                tile.Salt = DecodePolyIdSalt((dtPolyRef)lastRef);
            }

            if(tile == null)
                return Status.Failure | Status.OutOfMemory;

            // insert tile into the position
            int h = ComputeTileHash(header.X, header.Y, _tileLutMask);
            tile.Next = _posLookup[h];
            _posLookup[h] = tile;

            tile.Verts = data.NavVerts;
            tile.Polys = data.NavPolys;
            tile.Links = data.NavLinks;
            tile.DetailMeshes = data.NavDMeshes;
            tile.DetailVerts = data.NavDVerts;
            tile.DetailTris = data.NavDTris;
            tile.BVTree = data.NavBvTree;
            tile.OffMeshCons = data.OffMeshCons;

            tile.LinksFreeList = 0;
            tile.Links[header.MaxLinkCount - 1].Next = NullLink;
            for (uint i = 0; i < header.MaxLinkCount-1; i++)
            {
                tile.Links[i].Next = i + 1;
            }

            tile.Data = data;
            tile.Header = header;
            tile.Flags = flags;

            ConnectIntLinks(tile);
            BaseOffMeshLinks(tile);

            int MaxNeis = 32;
            MeshTile[] neis = new MeshTile[MaxNeis];
            int nneis;
            nneis = GetTilesAt(header.X, header.Y, ref neis, MaxNeis);
            for (int j = 0; j < nneis; j++)
            {
                MeshTile temp = neis[j];
                if (neis[j] != tile)
                {
                    ConnectExtLinks(ref tile, ref temp, -1);
                    ConnectExtLinks(ref temp, ref tile, -1);
                }
                ConnectExtOffMeshLinks(ref tile, ref temp, -1);
                ConnectExtOffMeshLinks(ref temp, ref tile, -1);
            }

            for (int i = 0; i < 8; i++)
            {
                nneis = GetNeighborTilesAt(header.X, header.Y, i, ref neis, MaxNeis);
                for (int j = 0; j < nneis; j++)
                {
                    MeshTile temp = neis[j];
                    ConnectExtLinks(ref tile, ref temp, i);
                    ConnectExtLinks(ref temp, ref tile, Helper.OppositeTile(i));
                    ConnectExtOffMeshLinks(ref tile, ref temp, i);
                    ConnectExtOffMeshLinks(ref temp, ref tile, Helper.OppositeTile(i));
                }
            }

            result = GetTileRef(tile);

            return Status.Success;
        }

        private int ComputeTileHash(int x, int y, int mask)
        {
            long h1 = 0x8da6b343;
            long h2 = 0xd8163841;
            long n = h1*x + h2*y;
            return (int) (n & mask);
        }

        public Status RemoveTile(dtTileRef refId, out NavMeshBuilder data)
        {
            data = null;
            if(refId == 0)
                return Status.Failure | Status.InvalidParam;
            var tileIndex = DecodePolyIdTile((dtPolyRef)refId);
            var tileSalt = DecodePolyIdSalt((dtPolyRef)refId);
            if(tileIndex >= _maxTiles)
                return Status.Failure | Status.InvalidParam;
            MeshTile tile = _tiles[tileIndex];
            if(tile.Salt != tileSalt)
                return Status.Failure | Status.InvalidParam;

            int h = ComputeTileHash(tile.Header.X, tile.Header.Y, _tileLutMask);
            MeshTile prev = null;
            MeshTile cur = _posLookup[h];
            while (cur != null)
            {
                if (cur == tile)
                {
                    if (prev != null)
                        prev.Next = cur.Next;
                    else
                    {
                        _posLookup[h] = cur.Next;
                    }
                    break;
                }
                prev = cur;
                cur = cur.Next;
            }

            int MaxNeis = 32;
            MeshTile[] neis = new MeshTile[MaxNeis];
            int nneis;

            nneis = GetTilesAt(tile.Header.X, tile.Header.Y, ref neis, MaxNeis);
            for (int j = 0; j < nneis; j++)
            {
                if (neis[j] == tile) continue;
                MeshTile temp = neis[j];
                UnconnectExtLinks(ref temp, ref tile);
            }

            for (int i = 0; i < 8; i++)
            {
                nneis = GetNeighborTilesAt(tile.Header.X, tile.Header.Y, i, ref neis, MaxNeis);
                for (int j = 0; j < nneis; j++)
                {
                    MeshTile temp = neis[j];
                    UnconnectExtLinks(ref temp, ref tile);
                }
            }

            // reset tile
            if ((tile.Flags & TileFreeData) != 0)
            {
                tile.Data = null;
            }
            else
            {
                data = tile.Data;
            }

            tile.Header = null;
            tile.Flags = 0;
            tile.LinksFreeList = 0;
            tile.Polys = null;
            tile.Verts = null;
            tile.Links = null;
            tile.DetailMeshes = null;
            tile.DetailVerts = null;
            tile.DetailTris = null;
            tile.BVTree = null;
            tile.OffMeshCons = null;

            tile.Salt = (dtPolyRef)((tile.Salt + 1) & ((1 << (int)_saltBits) - 1));
            if (tile.Salt == 0)
                tile.Salt++;

            tile.Next = _nextFree;
            _nextFree = tile;

            return Status.Success;
        }

        public void CalcTileLoc(float posx, float posy, float posz, out int tx, out int ty)
        {
            tx = (int) Math.Floor((posx - _orig[0])/_tileWidth);
            ty = (int) Math.Floor((posz - _orig[2])/_tileHeight);
        }

        public MeshTile GetTileAt(int x, int y, int layer)
        {
            int h = ComputeTileHash(x, y, _tileLutMask);
            MeshTile tile = _posLookup[h];
            while (tile != null)
            {
                if (tile.Header != null && tile.Header.X == x && tile.Header.Y == y && tile.Header.Layer == layer)
                    return tile;
                tile = tile.Next;
            }
            return null;
        }

        public int GetTilesAt(int x, int y, ref MeshTile[] tiles, int maxTiles)
        {
            int n = 0;

            int h = ComputeTileHash(x, y, _tileLutMask);
            MeshTile tile = _posLookup[h];
            while (tile != null)
            {
                if (tile.Header != null && tile.Header.X == x && tile.Header.Y == y)
                {
                    if (n < maxTiles)
                        tiles[n++] = tile;
                }
                tile = tile.Next;
            }
            return n;
        }

        public dtTileRef GetTileRefAt(int x, int y, int layer)
        {
            int h = ComputeTileHash(x, y, _tileLutMask);
            MeshTile tile = _posLookup[h];
            while (tile != null)
            {
                if (tile.Header != null && tile.Header.X == x && tile.Header.Y == y && tile.Header.Layer == layer)
                    return GetTileRef(tile);
                tile = tile.Next;
            }
            return 0;
        }

        public dtTileRef GetTileRef(MeshTile tile)
        {
            if (tile == null) return 0;
            uint? it = null;
            for (uint i = 0; i < _tiles.Length; i++)
            {
                if (_tiles[i] == tile)
                    it = i;
            }
            if (!it.HasValue)
                throw new Exception("Could not find matching tile");

            return (dtTileRef)EncodePolyId(tile.Salt, it.Value, 0);
        }

        public MeshTile GetTileByRef(dtTileRef refId)
        {
            if (refId == 0)
                return null;

            uint tileIndex = DecodePolyIdTile((dtPolyRef)refId);
            uint tileSalt = DecodePolyIdSalt((dtPolyRef)refId);
            if ((int) tileIndex >= _maxTiles)
                return null;
            MeshTile tile = _tiles[tileIndex];
            if (tile.Salt != tileSalt)
                return null;
            return tile;
        }

        public int GetMaxTiles()
        {
            return _maxTiles;
        }

        public MeshTile GetTile(int i)
        {
            return _tiles[i];
        }

        public Status GetTileAndPolyByRef(dtPolyRef refid, ref MeshTile tile, ref Poly poly)
        {
            if(refid == 0) return Status.Failure;
            DecodePolyId(refid, out var salt, out var it, out var ip);
            if(it >= _maxTiles) return Status.Failure|Status.InvalidParam;
            if(_tiles[it].Salt != salt || _tiles[it].Header == null) return Status.Failure | Status.InvalidParam;
            if(ip >= _tiles[it].Header.PolyCount) return Status.Failure | Status.InvalidParam;
            tile = _tiles[it];
            poly = _tiles[it].Polys[ip];
            return Status.Success;
        }

        public void GetTileAndPolyByRefUnsafe(dtPolyRef refId, out MeshTile tile, out Poly poly)
        {
            DecodePolyId(refId, out var salt, out var it, out var ip);
            tile = _tiles[it];
            poly = _tiles[it].Polys[ip];
        }

        public bool IsValidPolyRef(dtPolyRef refId)
        {
            if (refId == 0) return false;
            DecodePolyId(refId, out var salt, out var it, out var ip);
            if (it >= _maxTiles) return false;
            if (_tiles[it].Salt != salt || _tiles[it].Header == null) return false;
            if (ip >= _tiles[it].Header.PolyCount) return false;
            return true;
        }

        public uint GetPolyRefBase(MeshTile tile)
        {
            if (tile == null) return 0;
            uint? it = null;
            for (uint i = 0; i < _tiles.Length; i++)
            {
                if (_tiles[i] == tile)
                    it = i;
            }
            if (!it.HasValue)
                throw new Exception("Could not find matching tile");

            return EncodePolyId(tile.Salt, it.Value, 0);
        }

        public Status GetOffMeshConnectionPolyEndPoints(dtPolyRef prevRef, dtPolyRef polyRef, ref float[] startPos, ref float[] endPos)
        {
            if(polyRef == 0) return Status.Failure;

            DecodePolyId(polyRef, out var salt, out var it, out var ip);
            if (it >= _maxTiles) return Status.Failure | Status.InvalidParam;
            if (_tiles[it].Salt != salt || _tiles[it].Header == null) return Status.Failure | Status.InvalidParam;
            MeshTile tile = _tiles[it];
            if (ip >= _tiles[it].Header.PolyCount) return Status.Failure | Status.InvalidParam;

            Poly poly = tile.Polys[ip];

            if(poly.Type != NavMeshBuilder.PolyTypeOffMeshConnection)
                return Status.Failure;

            int idx0 = 0, idx1 = 1;

            for (long i = poly.FirstLink; i != NullLink; i = tile.Links[i].Next)
            {
                if (tile.Links[i].Edge == 0)
                {
                    if (tile.Links[i].Ref != prevRef)
                    {
                        idx0 = 1;
                        idx1 = 0;
                    }
                    break;
                }
            }

            Array.Copy(tile.Verts, poly.Verts[idx0] * 3, startPos, 0, 3);
            Array.Copy(tile.Verts, poly.Verts[idx1]*3, endPos, 0, 3);

            return Status.Success;
        }

        public OffMeshConnection GetOffMeshConnectionByRef(dtPolyRef refId)
        {
            if(refId == 0) return null;

            DecodePolyId(refId, out var salt, out var it, out var ip);
            if (it >= _maxTiles) return null;
            if (_tiles[it].Salt != salt || _tiles[it].Header == null) return null;
            MeshTile tile = _tiles[it];
            if (ip >= _tiles[it].Header.PolyCount) return null;

            Poly poly = tile.Polys[ip];

            if (poly.Type != NavMeshBuilder.PolyTypeOffMeshConnection)
                return null;

            long idx = ip - tile.Header.OffMeshBase;
            return tile.OffMeshCons[idx];
        }

        public Status SetPolyFlags(dtPolyRef refId, int flags)
        {
            if (refId == 0) return Status.Failure;

            DecodePolyId(refId, out var salt, out var it, out var ip);
            if (it >= _maxTiles) return Status.Failure | Status.InvalidParam;
            if (_tiles[it].Salt != salt || _tiles[it].Header == null) return Status.Failure | Status.InvalidParam;
            MeshTile tile = _tiles[it];
            if (ip >= _tiles[it].Header.PolyCount) return Status.Failure | Status.InvalidParam;

            Poly poly = tile.Polys[ip];
            poly.Flags = flags;
            return Status.Success;
        }

        public Status GetPolyFlags(dtPolyRef refId, ref int resultFlags)
        {if (refId == 0) return Status.Failure;

            DecodePolyId(refId, out var salt, out var it, out var ip);
            if (it >= _maxTiles) return Status.Failure | Status.InvalidParam;
            if (_tiles[it].Salt != salt || _tiles[it].Header == null) return Status.Failure | Status.InvalidParam;
            MeshTile tile = _tiles[it];
            if (ip >= _tiles[it].Header.PolyCount) return Status.Failure | Status.InvalidParam;

            Poly poly = tile.Polys[ip];
            resultFlags = poly.Flags;

            return Status.Success;
        }

        public Status SetPolyArea(dtPolyRef refId, short area)
        {
            if (refId == 0) return Status.Failure;

            DecodePolyId(refId, out var salt, out var it, out var ip);
            if (it >= _maxTiles) return Status.Failure | Status.InvalidParam;
            if (_tiles[it].Salt != salt || _tiles[it].Header == null) return Status.Failure | Status.InvalidParam;
            MeshTile tile = _tiles[it];
            if (ip >= _tiles[it].Header.PolyCount) return Status.Failure | Status.InvalidParam;

            Poly poly = tile.Polys[ip];
            poly.Area = area;
            return Status.Success;
        }

        public Status GetPolyArea(dtPolyRef refId, ref short resultArea)
        {
            if (refId == 0) return Status.Failure;

            DecodePolyId(refId, out var salt, out var it, out var ip);
            if (it >= _maxTiles) return Status.Failure | Status.InvalidParam;
            if (_tiles[it].Salt != salt || _tiles[it].Header == null) return Status.Failure | Status.InvalidParam;
            MeshTile tile = _tiles[it];
            if (ip >= _tiles[it].Header.PolyCount) return Status.Failure | Status.InvalidParam;

            Poly poly = tile.Polys[ip];
            resultArea = poly.Area;

            return Status.Success;
        }

        public Status StoreTileState(MeshTile tile, out TileState tileState)
        {
            tileState = new TileState();
            tileState.Magic = Helper.NavMeshMagic;
            tileState.Version = Helper.NavMeshVersion;
            tileState.Ref = GetTileRef(tile);
            tileState.PolyStates = new PolyState[tile.Header.PolyCount];
            for (int i = 0; i < tile.Header.PolyCount; i++)
            {
                Poly p = tile.Polys[i];
                tileState.PolyStates[i] = new PolyState();
                tileState.PolyStates[i].Flags = p.Flags;
                tileState.PolyStates[i].Area = p.Area;
            }

            return Status.Success;
        }

        public Status RestoreTileState(MeshTile tile, TileState tileState)
        {
            if (tileState.Magic != Helper.NavMeshMagic)
                return Status.Failure | Status.WrongMagic;
            if (tileState.Version != Helper.NavMeshVersion)
                return Status.Failure | Status.WrongVersion;
            if (tileState.Ref != GetTileRef(tile))
                return Status.Failure | Status.InvalidParam;

            for (int i = 0; i < tile.Header.PolyCount; i++)
            {
                Poly p = tile.Polys[i];
                PolyState s = tileState.PolyStates[i];
                p.Flags = s.Flags;
                p.Area = s.Area;
            }

            return Status.Success;
        }

        public dtPolyRef EncodePolyId(uint salt, uint it, uint ip)
        {
#if DT_POLYREF64
#else
            return ((dtPolyRef)salt << (int)(_polyBits + _tileBits)) | ((dtPolyRef)it << (int)_polyBits) | (dtPolyRef)ip;
#endif
        }

        public void DecodePolyId(dtPolyRef refId, out uint salt, out uint it, out uint ip)
        {
            dtPolyRef saltMask = (dtPolyRef)(1 << (int)_saltBits) - 1;
            dtPolyRef tileMask = (dtPolyRef)(1 << (int)_tileBits) - 1;
            dtPolyRef polyMask = (dtPolyRef)(1 << (int)_polyBits) - 1;

            salt = ((refId >> (int)(_polyBits + _tileBits)) & saltMask);
            it = ((refId >> (int)_polyBits) & tileMask);
            ip = (refId & polyMask);
        }

        public uint DecodePolyIdSalt(dtPolyRef refId)
        {
#if DT_POLYREF64
#else
            dtPolyRef saltMask = ((dtPolyRef)1 << (int)_saltBits) - 1;
            return (uint)((refId >> (int)(_polyBits + _tileBits)) & saltMask);
#endif
        }

        public uint DecodePolyIdTile(dtPolyRef refId)
        {
#if DT_POLYREF64
#else
            dtPolyRef tileMask = ((dtPolyRef)1 << (int)_tileBits) - 1;
            return (uint)((refId >> (int)_polyBits) & tileMask);
#endif
        }

        public uint DecodePolyIdPoly(dtPolyRef refId)
        {
#if DT_POLYREF64
#else
            dtPolyRef polyMask = ((dtPolyRef)1 << (int)_polyBits) - 1;
            return (uint)(refId & polyMask);
#endif
        }

        private int GetNeighborTilesAt(int x, int y, int side, ref MeshTile[] tiles, int maxTiles)
        {
            int nx = x, ny = y;
            switch (side)
            {
                case 0:
                    nx++;
                    break;
                case 1:
                    nx++;
                    ny++;
                    break;
                case 2:
                    ny++;
                    break;
                case 3:
                    nx--;
                    ny++;
                    break;
                case 4:
                    nx--;
                    break;
                case 5:
                    nx--;
                    ny--;
                    break;
                case 6:
                    ny--;
                    break;
                case 7:
                    nx++;
                    ny--;
                    break;
            }

            return GetTilesAt(nx, ny, ref tiles, maxTiles);
        }

        private int FindConnectingPolys(float vax, float vay, float vaz, float vbx, float vby, float vbz, MeshTile tile,
                                        int side, ref dtPolyRef[] con, ref float[] conarea, int maxcon)
        {
            if (tile == null) return 0;
            float[] amin = new float[2], amax = new float[2];
            Helper.CalcSlabEndPoints(vax, vay, vaz, vbx, vby, vbz, ref amin, ref amax, side);
            float apos = Helper.GetSlabCoord(vax, vay, vaz, side);

            float[] bmin = new float[2], bmax = new float[2];
            int m = NavMeshBuilder.ExtLink | side;
            int n = 0;
            var baseId = GetPolyRefBase(tile);

            for (int i = 0; i < tile.Header.PolyCount; i++)
            {
                Poly poly = tile.Polys[i];
                int nv = poly.VertCount;
                for (int j = 0; j < nv; j++)
                {
                    if (poly.Neis[j] != m) continue;

                    int vc = poly.Verts[j]*3;
                    int vd = poly.Verts[(j + 1)%nv]*3;
                    float bpos = Helper.GetSlabCoord(tile.Verts[vc + 0], tile.Verts[vc + 1], tile.Verts[vc + 2], side);

                    if (Math.Abs(apos - bpos) > 0.01f)
                        continue;

                    Helper.CalcSlabEndPoints(tile.Verts[vc + 0], tile.Verts[vc + 1], tile.Verts[vc + 2], tile.Verts[vd + 0], tile.Verts[vd + 1], tile.Verts[vd + 2], ref bmin, ref bmax, side);

                    if (!Helper.OverlapSlabs(amin, amax, bmin, bmax, 0.01f, tile.Header.WalkableClimb)) continue;

                    if (n < maxcon)
                    {
                        conarea[n*2 + 0] = Math.Max(amin[0], bmin[0]);
                        conarea[n*2 + 1] = Math.Min(amax[0], bmax[0]);
                        con[n] = baseId | (dtPolyRef)i;
                        n++;
                    }
                    break;
                }
            }
            return n;
        }

        private void ConnectIntLinks(MeshTile tile)
        {
            if (tile == null) return;
            var baseId = GetPolyRefBase(tile);

            for (int i = 0; i < tile.Header.PolyCount; i++)
            {
                Poly poly = tile.Polys[i];
                poly.FirstLink = NullLink;

                if (poly.Type == NavMeshBuilder.PolyTypeOffMeshConnection)
                    continue;

                for (int j = poly.VertCount-1; j >= 0; j--)
                {
                    if (poly.Neis[j] == 0 || (poly.Neis[j] & NavMeshBuilder.ExtLink) != 0) continue;

                    var idx = AllocLink(tile);
                    if (idx != NullLink)
                    {
                        Link link = tile.Links[idx];
                        link.Ref = baseId | (dtPolyRef)(poly.Neis[j] - 1);
                        link.Edge = (short) j;
                        link.Side = 0xff;
                        link.BMin = link.BMax = 0;

                        link.Next = poly.FirstLink;
                        poly.FirstLink = idx;
                    }
                }
            }
        }

        private void BaseOffMeshLinks(MeshTile tile)
        {
            if (tile == null) return;
            var baseId = GetPolyRefBase(tile);

            for (int i = 0; i < tile.Header.OffMeshConCount; i++)
            {
                OffMeshConnection con = tile.OffMeshCons[i];
                Poly poly = tile.Polys[con.Poly];

                var ext = new Vector3(con.Rad, tile.Header.WalkableClimb, con.Rad);
                int p = 0;
                var refId = FindNearestPolyInTile(tile, V3(con.Pos, p), ext, out var nearestPt);
                if (refId <= 0) continue;
                if (((nearestPt.X - con.Pos[p + 0])*(nearestPt.X - con.Pos[p + 0])) +
                    ((nearestPt.Z - con.Pos[p + 2])*(nearestPt.Z - con.Pos[p + 2])) > (con.Rad*con.Rad))
                {
                    continue;
                }

                int v = poly.Verts[0]*3;
                float[] tmp = new float[3];
                CopyTo(nearestPt, ref tmp, v);
                tile.Verts = tmp;

                var idx = AllocLink(tile);
                if (idx != NullLink)
                {
                    Link link = tile.Links[idx];
                    link.Ref = refId;
                    link.Edge = 0;
                    link.Side = 0xff;
                    link.BMin = link.BMax = 0;
                    link.Next = poly.FirstLink;
                    poly.FirstLink = idx;
                }

                var tidx = AllocLink(tile);
                if (tidx != NullLink)
                {
                    var landPolyIdx = (ushort) DecodePolyIdPoly(refId);
                    Poly landPoly = tile.Polys[landPolyIdx];
                    Link link = tile.Links[tidx];
                    link.Ref = baseId | (dtPolyRef)con.Poly;
                    link.Edge = 0xff;
                    link.Side = 0xff;
                    link.BMin = link.BMax = 0;
                    link.Next = landPoly.FirstLink;
                    landPoly.FirstLink = tidx;
                }
            }
        }

        private void ConnectExtLinks(ref MeshTile tile, ref MeshTile target, int side)
        {
            if (tile == null) return;

            for (int i = 0; i < tile.Header.PolyCount; i++)
            {
                Poly poly = tile.Polys[i];

                int nv = poly.VertCount;
                for (int j = 0; j < nv; j++)
                {
                    if ((poly.Neis[j] & NavMeshBuilder.ExtLink) == 0)
                    {
                        continue;
                    }

                    int dir = (int) (poly.Neis[j] & 0xff);
                    if (side != -1 && dir != side)
                        continue;

                    int va = poly.Verts[j]*3;
                    int vb = poly.Verts[(j + 1)%nv]*3;
                    var nei = new dtPolyRef[4];
                    float[] neia = new float[4*2];
                    int nnei = FindConnectingPolys(tile.Verts[va + 0], tile.Verts[va + 1], tile.Verts[va + 2],
                                                   tile.Verts[vb + 0], tile.Verts[vb + 1], tile.Verts[vb + 2], target,
                                                   Helper.OppositeTile(dir), ref nei, ref neia, 4);
                    for (int k = 0; k < nnei; k++)
                    {
                        var idx = AllocLink(tile);
                        if (idx != NullLink)
                        {
                            Link link = tile.Links[idx];
                            link.Ref = nei[k];
                            link.Edge = (short)j;
                            link.Side = (short) dir;

                            link.Next = poly.FirstLink;
                            poly.FirstLink = idx;

                            if (dir == 0 || dir == 4)
                            {
                                float tmin = (neia[k*2 + 0] - tile.Verts[va + 2])/
                                             (tile.Verts[vb + 2] - tile.Verts[va + 2]);
                                float tmax = (neia[k * 2 + 1] - tile.Verts[va + 2]) /
                                             (tile.Verts[vb + 2] - tile.Verts[va + 2]);
                                if (tmin > tmax)
                                {
                                    float temp = tmin;
                                    tmin = tmax;
                                    tmax = temp;
                                }
                                link.BMin = (short) (Math.Min(1.0f, Math.Max(tmin, 0.0f))*255.0f);
                                link.BMax = (short) (Math.Min(1.0f, Math.Max(tmax, 0.0f))*255.0f);
                            }
                            else if (dir == 2 || dir == 6)
                            {
                                float tmin = (neia[k * 2 + 0] - tile.Verts[va + 0]) /
                                             (tile.Verts[vb + 0] - tile.Verts[va + 0]);
                                float tmax = (neia[k * 2 + 1] - tile.Verts[va + 0]) /
                                             (tile.Verts[vb + 0] - tile.Verts[va + 0]);
                                if (tmin > tmax)
                                {
                                    float temp = tmin;
                                    tmin = tmax;
                                    tmax = temp;
                                }
                                link.BMin = (short)(Math.Min(1.0f, Math.Max(tmin, 0.0f)) * 255.0f);
                                link.BMax = (short)(Math.Min(1.0f, Math.Max(tmax, 0.0f)) * 255.0f);                                
                            }
                        }
                    }
                }
            }
        }

        private uint AllocLink(MeshTile tile)
        {
            if (tile.LinksFreeList == NullLink)
                return NullLink;
            long link = tile.LinksFreeList;
            tile.LinksFreeList = tile.Links[link].Next;
            if (link < uint.MinValue || link > uint.MaxValue)
                throw new Exception($"expecting all MeshTile.LinksFreeList to be inside uint value, but a value of {link} was encountered");
            return (uint)link;
        }

        private void ConnectExtOffMeshLinks(ref MeshTile tile, ref MeshTile target, int side)
        {
            if (tile == null) return;

            short oppositeSide = (side == -1) ? (short)0xff : (short) Helper.OppositeTile(side);
            for (int i = 0; i < target.Header.OffMeshConCount; i++)
            {
                OffMeshConnection targetCon = target.OffMeshCons[i];
                if (targetCon.Side != oppositeSide) continue;

                Poly targetPoly = target.Polys[targetCon.Poly];
                if (targetPoly.FirstLink == NullLink)
                    continue;

                var ext = new Vector3(targetCon.Rad, target.Header.WalkableClimb, targetCon.Rad);

                int p = 3;
                var refId = FindNearestPolyInTile(tile, V3(targetCon.Pos, p), ext, out var nearestPt);
                if (refId <= 0)
                    continue;

                if(((nearestPt.X-targetCon.Pos[p + 0])*(nearestPt.X-targetCon.Pos[p + 0]))+((nearestPt.Z-targetCon.Pos[p + 2])*(nearestPt.Z-targetCon.Pos[p + 2])) > (targetCon.Rad*targetCon.Rad))
                    continue;

                int v = targetPoly.Verts[1]*3;
                float[] tmp = new float[3];
                CopyTo(nearestPt, ref tmp, v);
                target.Verts = tmp;

                var idx = AllocLink(target);
                if (idx != NullLink)
                {
                    Link link = target.Links[idx];
                    link.Ref = refId;
                    link.Edge = 1;
                    link.Side = oppositeSide;
                    link.BMin = link.BMax = 0;

                    link.Next = targetPoly.FirstLink;
                    targetPoly.FirstLink = idx;
                }

                if ((targetCon.Flags & NavMeshBuilder.OffMeshConBiDir) != 0)
                {
                    var tidx = AllocLink(tile);
                    if (tidx != NullLink)
                    {
                        int landPolyIdx = (int) DecodePolyIdPoly(refId);
                        Poly landPoly = tile.Polys[landPolyIdx];
                        Link link = tile.Links[tidx];
                        link.Ref = GetPolyRefBase(target) | (dtPolyRef)(targetCon.Poly);
                        link.Edge = 0xff;
                        link.Side = side == -1 ? (short)0xff : (short)side;
                        link.BMin = link.BMax = 0;
                        link.Next = landPoly.FirstLink;
                        landPoly.FirstLink = tidx;
                    }
                }
            }
        }

        private void UnconnectExtLinks(ref MeshTile tile, ref MeshTile target)
        {
            if (tile == null || target == null) return;

            var targetNum = DecodePolyIdTile(GetTileRef(target));

            for (int i = 0; i < tile.Header.PolyCount; i++)
            {
                Poly poly = tile.Polys[i];
                var j = poly.FirstLink;
                var pj = NullLink;
                while (j != NullLink)
                {
                    if (tile.Links[j].Side != 0xff && DecodePolyIdTile(tile.Links[j].Ref) == targetNum)
                    {
                        var nj = tile.Links[j].Next;
                        if (pj == NullLink)
                            poly.FirstLink = nj;
                        else
                        {
                            tile.Links[pj].Next = nj;
                        }
                        FreeLink(ref tile, j);
                        j = nj;
                    }
                    else
                    {
                        pj = j;
                        j = tile.Links[j].Next;
                    }
                }
            }
        }

        private void FreeLink(ref MeshTile tile, uint link)
        {
            tile.Links[link].Next = tile.LinksFreeList;
            tile.LinksFreeList = link;
        }

        private int QueryPolygonsInTile(MeshTile tile, Vector3 qmin, Vector3 qmax, ref dtPolyRef[] polys, int maxPolys)
        {
            if (tile.BVTree != null)
            {
                int node = 0;
                int end = tile.Header.BVNodeCount;
                var tbmin = V3(tile.Header.BMin, 0);
                var tbmax = V3(tile.Header.BMax, 0);
                float qfac = tile.Header.BVQuantFactor;

                var min = Vector3.Min(tbmax, Vector3.Max(qmin, tbmin)) - tbmin;
                var max = Vector3.Min(tbmax, Vector3.Max(qmax, tbmin)) - tbmin;

                var bmin = new Vector3i((int)(qfac * min.X) & 0xfffe,
                    (int)(qfac * min.Y) & 0xfffe,
                    (int)(qfac * min.Z) & 0xfffe);
                var bmax = new Vector3i((int)(qfac * max.X + 1) | 1,
                    (int)(qfac * max.Y + 1) | 1,
                    (int)(qfac * max.Z + 1) | 1);

                var baseId = GetPolyRefBase(tile);
                int n = 0;
                while (node < end)
                {
                    bool overlap = Helper.OverlapQuantBounds(bmin, bmax, tile.BVTree[node].BMin, tile.BVTree[node].BMax);
                    bool isLeafNode = tile.BVTree[node].I >= 0;

                    if (isLeafNode && overlap)
                    {
                        if (n < maxPolys)
                            polys[n++] = baseId | (dtPolyRef)tile.BVTree[node].I;
                    }

                    if (overlap || isLeafNode)
                        node++;
                    else
                    {
                        int escapeIndex = -tile.BVTree[node].I;
                        node += escapeIndex;
                    }
                }
                return n;
            }
            else
            {
                var bmin = new Vector3();
                var bmax = new Vector3();
                int n = 0;
                var baseId = GetPolyRefBase(tile);
                for (int i = 0; i < tile.Header.PolyCount; i++)
                {
                    Poly p = tile.Polys[i];
                    if (p.Type == NavMeshBuilder.PolyTypeOffMeshConnection)
                        continue;

                    int v = p.Verts[0*3];
                    CopyTo(tile.Verts, v, ref bmin);
                    CopyTo(tile.Verts, v, ref bmax);
                    for (int j = 1; j < p.VertCount; j++)
                    {
                        v = p.Verts[j]*3;
                        bmin = Vector3.Min(bmin, V3(tile.Verts, v));
                        bmax = Vector3.Max(bmax, V3(tile.Verts, v));
                    }
                    if (Helper.OverlapBounds(qmin, qmax, bmin, bmax))
                    {
                        if (n < maxPolys)
                            polys[n++] = baseId | (dtPolyRef)i;
                    }
                }
                return n;
            }
        }

        private dtPolyRef FindNearestPolyInTile(MeshTile tile, Vector3 center, Vector3 extents, out Vector3 nearestPt)
        {
            var bmin = center - extents;
            var bmax = center + extents;

            var polys = new dtPolyRef[128];
            int polyCount = QueryPolygonsInTile(tile, bmin, bmax, ref polys, 128);

            dtPolyRef nearest = 0;
            float nearestDistanceSqr = float.MaxValue;
            nearestPt = Vector3.Zero;
            for (int i = 0; i < polyCount; i++)
            {
                var refId = polys[i];
                var closestPtPoly = new Vector3();
                ClosestPointOnPolyInTile(tile, DecodePolyIdPoly(refId), center, ref closestPtPoly);
                float d = Vector3.DistanceSquared(center, closestPtPoly);
                if (d < nearestDistanceSqr)
                {
                    nearestPt = closestPtPoly;
                    nearestDistanceSqr = d;
                    nearest = refId;
                }
            }
            return nearest;
        }

        private void ClosestPointOnPolyInTile(MeshTile tile, long ip, Vector3 pos, ref Vector3 closestPt)
        {
            Poly poly = tile.Polys[ip];
            if (poly.Type == NavMeshBuilder.PolyTypeOffMeshConnection)
            {
                int v0 = poly.Verts[0]*3;
                int v1 = poly.Verts[1]*3;
                float d0 = Vector3.Distance(pos, V3(tile.Verts, v0));
                float d1 = Vector3.Distance(pos, V3(tile.Verts, v1));
                float u = d0/(d0 + d1);
                Helper.VLerp(ref closestPt, tile.Verts[v0 + 0], tile.Verts[v0 + 1], tile.Verts[v0 + 2], tile.Verts[v1 + 0], tile.Verts[v1 + 1], tile.Verts[v1 + 2], u);
                return;
            }

            PolyDetail pd = tile.DetailMeshes[ip];

            float[] verts = new float[NavMeshBuilder.VertsPerPoly*3];
            float[] edged = new float[NavMeshBuilder.VertsPerPoly];
            float[] edget = new float[NavMeshBuilder.VertsPerPoly];
            int nv = poly.VertCount;
            for (int i = 0; i < nv; i++)
            {
                Array.Copy(tile.Verts, poly.Verts[i]*3, verts, i*3, 3);
            }

            closestPt = pos;

            if (!Helper.DistancePtPolyEdgesSqr(pos, verts, nv, ref edged, ref edget))
            {
                float dmin = float.MaxValue;
                int imin = -1;
                for (int i = 0; i < nv; i++)
                {
                    if (edged[i] < dmin)
                    {
                        dmin = edged[i];
                        imin = i;
                    }
                }
                int va = imin*3;
                int vb = ((imin + 1)%nv)*3;
                Helper.VLerp(ref closestPt, verts[va + 0], verts[va + 1], verts[va + 2], verts[vb + 0], verts[vb + 1], verts[vb + 2], edget[imin]);
            }

            for (int j = 0; j < pd.TriCount; j++)
            {
                int t = (int)(pd.TriBase + j)*4;
                float[] v = new float[9];
                for (int k = 0; k < 3; k++)
                {
                    if (tile.DetailTris[t + k] < poly.VertCount)
                    {
                        Array.Copy(tile.Verts, poly.Verts[tile.DetailTris[t + k]] * 3, v, k*3, 3);
                        //v[k] = tile.Verts[poly.Verts[tile.DetailTris[t + k]]*3];
                    }
                    else
                    {
                        Array.Copy(tile.DetailVerts, (pd.VertBase + (tile.DetailTris[t + k] - poly.VertCount)) * 3, v, k*3, 3);
                        //v[k] = tile.DetailVerts[(pd.VertBase + (tile.DetailTris[t + k] - poly.VertCount))*3];
                    }
                }
                float h = 0;
                if (Helper.ClosestHeightPointTriangle(pos, v, ref h))
                {
                    closestPt.X = h;
                    break;
                }
            }
        }
    }
}
