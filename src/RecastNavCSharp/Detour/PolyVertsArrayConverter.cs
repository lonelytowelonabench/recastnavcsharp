﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Detour
{
    internal class PolyVertsArrayConverter : JsonConverter<PolyVerts>
    {
        public override PolyVerts Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartArray)
                throw new JsonException();
            var polyVerts = new PolyVerts();
            for(var i = 0; i < dtPoly.DT_VERTS_PER_POLYGON; i++)
            {
                reader.Read();
                polyVerts[i] = reader.GetUInt16();
            }

            //finish the array.
            reader.Read();
            return polyVerts;
        }

        public override void Write(Utf8JsonWriter writer, PolyVerts value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}