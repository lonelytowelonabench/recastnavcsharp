﻿using System;
using System.Numerics;
using System.Text.Json.Serialization;

using dtPolyRef = System.UInt32;

namespace Detour
{
    [Serializable]
    public class Node
    {
        [JsonConverter(typeof(Vector3ArrayConverter))]
        public Vector3 Pos { get; set; }
        public float Cost { get; set; }
        public float Total { get; set; }
        public uint PIdx { get; set; }  //Index to parent node
        public long Flags { get; set; }
        public dtPolyRef Id { get; set; }

        public static int NullIdx = ~0;
        public static int NodeOpen = 0x01;
        public static int NodeClosed = 0x02;

        public Node() { }
    }
}
