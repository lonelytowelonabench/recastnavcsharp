﻿using System;
using System.Runtime.InteropServices;
using System.Text.Json.Serialization;

namespace Detour
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    unsafe struct dtPoly
    {
        public const int DT_VERTS_PER_POLYGON = 6;

        public uint FirstLink;
        public fixed int Verts[DT_VERTS_PER_POLYGON];
        public fixed int Neis[DT_VERTS_PER_POLYGON];
        public int Flags;
        public short VertCount;
        public short _areaAndType;
    }

    [Serializable]
    public class Poly
    {
        public uint FirstLink { get; set; }
        [JsonInclude]
        public PolyVerts Verts;
        [JsonInclude]
        public PolyVerts Neis;
        public int Flags { get; set; }
        public short VertCount { get; set; }
        public short _areaAndType;

        public short Area
        {
            get { return (short)(_areaAndType & 0x3f); }
            set { _areaAndType = (short)((_areaAndType & 0xc0) | (value & 0x3f)); }
        }

        public short Type
        {
            get { return (short)(_areaAndType >> 6); }
            set { _areaAndType = (short) ((_areaAndType & 0x3f) | (value << 6)); }
        }

        public Poly()
        {
            Verts = new PolyVerts();
            Neis = new PolyVerts();
        }
    }

    [JsonConverter(typeof(PolyVertsArrayConverter))]
    public struct PolyVerts
    {
        public ushort _0;
        public ushort _1;
        public ushort _2;
        public ushort _3;
        public ushort _4;
        public ushort _5;

        public ushort this[int idx]
        {
            get
            {
                //switch statements are faster due to being compiled as a jump table.
                switch (idx)
                {
                    case 0: return _0;
                    case 1: return _1;
                    case 2: return _2;
                    case 3: return _3;
                    case 4: return _4;
                    case 5: return _5;
                    default: throw new ArgumentOutOfRangeException();
                }
            }

            set
            {
                switch (idx)
                {
                    case 0: _0 = value; break;
                    case 1: _1 = value; break;
                    case 2: _2 = value; break;
                    case 3: _3 = value; break;
                    case 4: _4 = value; break;
                    case 5: _5 = value; break;
                    default: throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
