﻿using System;

using dtTileRef = System.UInt32;

namespace Detour
{
    [Serializable]
    public class TileState
    {
        public int Magic { get; set; }
        public int Version { get; set; }
        public dtTileRef Ref { get; set; }

        public PolyState[] PolyStates { get; set; }
    }
}
