﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
#if DT_POLYREF64
using dtPolyRef = System.UInt64;
#else
using dtPolyRef = System.UInt32;
#endif

namespace Detour
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct dtLink
    {
        public dtPolyRef @ref;
        public uint next;
        public byte edge;
        public byte side;
        public byte bmin;
        public byte bmax;
    };


    [Serializable]
    public class Link
    {
        //public PolyRef Ref { get; set; }
        public dtPolyRef Ref { get; set; }
        public uint Next { get; set; }
        public short Edge { get; set; }
        public short Side { get; set; }
        public short BMin { get; set; }
        public short BMax { get; set; }
    }
}
