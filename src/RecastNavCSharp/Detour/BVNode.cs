﻿using System;
using System.Runtime.InteropServices;
using System.Text.Json.Serialization;

namespace Detour
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    unsafe struct dtBVNode
    {
        public const int BMinSize = 3;
        public const int BMaxSize = 3;

        public fixed int BMin[BMinSize];
        public fixed int BMax[BMaxSize];
        public int I;
    }

    [Serializable]
    public class BVNode
    {
        public Vector3i BMin { get; set; }
        public Vector3i BMax { get; set; }
        public int I { get; set; }

        public BVNode() { }

        public BVNode(Vector3i bMin, Vector3i bMax, int i)
        {
            BMin = bMin;
            BMax = bMax;
            I = i;
        }

        public BVNode(int[] bMin, int[] bMax, int i)
        {
            BMin = new Vector3i(bMin);
            BMax = new Vector3i(bMax);
            I = i;
        }
    }

    [JsonConverter(typeof(Vector3iArrayConverter))]
    public struct Vector3i
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public Vector3i(int[] arr)
        {
            X = arr[0];
            Y = arr[1];
            Z = arr[2];
        }

        public Vector3i(int x, int y, int z)
        {
            X = x; Y = y; Z = z;
        }
    }
}
