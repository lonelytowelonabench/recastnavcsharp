﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;
using Detour;
using dtPolyRef = System.UInt32;

namespace Recast.Data
{
    public static class Helper
    {
        public static int NavMeshMagic = 1;
        public static int NavMeshVersion = 1;
        public static int StatusDetailMast = 0x0fffffff;
        private static int[] offsetX = { -1, 0, 1, 0 };
        private static int[] offsetY = { 0, 1, 0, -1 };
        
        public static int GetDirOffsetX(int dir)
        {
            return offsetX[dir & 0x03];
        }

        public static int GetDirOffsetY(int dir)
        {
            return offsetY[dir & 0x03];
        }

        public static uint NextPow2(uint v)
        {
            v--;
            v |= v >> 1;
            v |= v >> 2;
            v |= v >> 4;
            v |= v >> 8;
            v |= v >> 16;
            v++;
            return v;
        }

        public static uint Ilog2(uint v)
        {
            uint r;
            uint shift;
            r = (v > 0xffff) ? 1u << 4 : 0 << 4;
            v >>= (int)r;
            shift = (v > 0xff) ? 1u << 3 : 0 << 3; v >>= (int)shift; r |= shift;
            shift = (v > 0xf) ? 1u << 2 : 0 << 2; v >>= (int)shift; r |= shift;
            shift = (v > 0x3) ? 1u << 1 : 0 << 1; v >>= (int)shift; r |= shift;
            r |= (v >> 1);
            return r;
        }

        public static float GetSlabCoord(float vax, float vay, float vaz, int side)
        {
            if (side == 0 || side == 4)
                return vax;
            if (side == 2 || side == 6)
                return vaz;
            return 0;
        }

        public static void CalcSlabEndPoints(float vax, float vay, float vaz, float vbx, float vby, float vbz, ref float[] bmin, ref float[] bmax, int side)
        {
            if (side == 0 || side == 4)
            {
                if (vaz < vbz)
                {
                    bmin[0] = vaz;
                    bmin[1] = vay;
                    bmax[0] = vbz;
                    bmax[1] = vby;
                }
                else
                {
                    bmin[0] = vbz;
                    bmin[1] = vby;
                    bmax[0] = vaz;
                    bmax[1] = vay;
                }
            }
            else if (side == 2 || side == 6)
            {
                if (vax < vbx)
                {
                    bmin[0] = vax;
                    bmin[1] = vay;
                    bmax[0] = vbx;
                    bmax[1] = vby;
                }
                else
                {
                    bmin[0] = vbx;
                    bmin[1] = vby;
                    bmax[0] = vax;
                    bmax[1] = vay;
                }
            }
        }

        public static bool OverlapSlabs(float[] amin, float[] amax, float[] bmin, float[] bmax, float px, float py)
        {
            float minx = Math.Max(amin[0] + px, bmin[0] + px);
            float maxx = Math.Min(amax[0] - px, bmax[0] - px);
            if (minx > maxx)
                return false;

            float ad = (amax[1] - amin[1])/(amax[0] - amin[0]);
            float ak = amin[1] - ad*amin[0];
            float bd = (bmax[1] - bmin[1])/(bmax[0] - bmin[0]);
            float bk = bmin[1] - bd*bmin[0];
            float aminy = ad*minx + ak;
            float amaxy = ad*maxx + ak;
            float bminy = bd*minx + bk;
            float bmaxy = bd*maxx + bk;
            float dmin = bminy - aminy;
            float dmax = bmaxy - amaxy;

            if (dmin*dmax < 0)
                return true;

            float thr = (py*2)*(py*2);
            if (dmin*dmin <= thr || dmax*dmax <= thr)
                return true;

            return false;
        }

        public static int OppositeTile(int side)
        {
            return (side + 4) & 0x7;
        }

        public static float VDist(float v1x, float v1y, float v1z, float v2x, float v2y, float v2z)
        {
            float dx = v2x - v1x;
            float dy = v2y - v1y;
            float dz = v2z - v1z;
            return (float)Math.Sqrt(dx*dx + dy*dy + dz*dz);
        }

        public static void VLerp(ref float[] dest, float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, float t)
        {
            dest[0] = v1x + (v2x - v1x)*t;
            dest[1] = v1y + (v2y - v1y)*t;
            dest[2] = v1z + (v2z - v1z)*t;
        }

        public static void VLerp(ref Vector3 dest, float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, float t)
        {
            dest = new Vector3(v1x + (v2x - v1x) * t,
                v1y + (v2y - v1y) * t,
                v1z + (v2z - v1z) * t);
        }

        public static bool DistancePtPolyEdgesSqr(Vector3 pt, float[] verts, int nverts, ref float[] ed, ref float[] et)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = nverts-1; i < nverts; j=i++)
            {
                int vi = i*3;
                int vj = j*3;
                if (((verts[vi + 2] > pt.Z) != (verts[vj + 2] > pt.Z)) &&
                    (pt.X < (verts[vj + 0] - verts[vi + 0])*(pt.Z - verts[vi + 2])/(verts[vj + 2] - verts[vi + 2]) + verts[vi + 0]))
                    c = !c;
                ed[j] = DistancePtSegSqr2D(pt, V3(verts, vj), V3(verts, vi), ref et, j);
            }

            return c;
        }

        public static float DistancePtSegSqr2D(Vector3 pt, Vector3 p, Vector3 q, ref float[] et, int t)
        {
            float pqx = q.X - p.X;
            float pqz = q.Z - p.Z;
            float dx = pt.X - p.X;
            float dz = pt.Z - p.Z;
            float d = pqx*pqx + pqz*pqz;
            et[t] = pqx*dx + pqz*dz;
            if (d > 0) et[t] /= d;
            if (et[t] < 0) et[t] = 0;
            else if(et[t] > 1) et[t] = 1;
            dx = p.X + et[t]*pqx - pt.X;
            dz = p.Z + et[t]*pqz - pt.Z;
            return dx*dx + dz*dz;
        }

        public static float DistancePtSegSqr2D(Vector3 pt, Vector3 p, Vector3 q, out float t)
        {
            float pqx = q.X - p.X;
            float pqz = q.Z - p.Z;
            float dx = pt.X - p.X;
            float dz = pt.Z - p.Z;
            float d = pqx * pqx + pqz * pqz;
            t = pqx * dx + pqz * dz;
            if (d > 0) t /= d;
            if (t < 0) t = 0;
            else if (t > 1) t = 1;
            dx = p.X + t * pqx - pt.X;
            dz = p.Z + t * pqz - pt.Z;
            return dx * dx + dz * dz;
        }

        public static float DistancePtSegSqr2D(float ptx, float pty, float ptz, float px, float py, float pz, float qx, float qy, float qz, out float t)
        {
            float pqx = qx - px;
            float pqz = qz - pz;
            float dx = ptx - px;
            float dz = ptz - pz;
            float d = pqx * pqx + pqz * pqz;
            t = pqx * dx + pqz * dz;
            if (d > 0) t /= d;
            if (t < 0) t = 0;
            else if (t > 1) t = 1;
            dx = px + t * pqx - ptx;
            dz = pz + t * pqz - ptz;
            return dx * dx + dz * dz;
        }

        public static bool ClosestHeightPointTriangle(Vector3 p, float[] t, ref float h)
        {
            if (t.Length != 9)
                throw new ArgumentOutOfRangeException(nameof(t));

            return ClosestHeightPointTriangle(p, V3(t, 0), V3(t, 3), V3(t, 6), ref h);
        }

        public static bool ClosestHeightPointTriangle(Vector3 p, Vector3 a, Vector3 b, Vector3 c, ref float h)
        {
            var v0 = Vector3.Subtract(c, a);
            var v1 = Vector3.Subtract(b, a);
            var v2 = Vector3.Subtract(p, a);

            float dot00 = VDot2D(v0, v0);
            float dot01 = VDot2D(v0, v1);
            float dot02 = VDot2D(v0, v2);
            float dot11 = VDot2D(v1, v1);
            float dot12 = VDot2D(v1, v2);

            float invDenom = 1.0f/(dot00*dot11 - dot01*dot01);
            float u = (dot11*dot02 - dot01*dot12)*invDenom;
            float v = (dot00*dot12 - dot01*dot02)*invDenom;

            float EPS = 1e-4f;
            if (u >= -EPS && v >= -EPS && (u + v) <= 1 + EPS)
            {
                h = a.Y + v0.Y*u + v1.Y*v;
                return true;
            }
            return false;
        }

        public static float VDot2D(float[] u, float[] v)
        {
            return u[0]*v[0] + u[2]*v[2];
        }

        public static float VDot2D(Vector3 u, Vector3 v)
        {
            return u.X*v.X + u.Z*v.Z;
        }

        internal static Vector3 VMad(Vector3 v1, Vector3 v2, float s)
        {
            return v1 + v2 * s;
        }

        public static void VMad(ref float[] dest, float[] v1, float[] v2, float s)
        {
            dest[0] = v1[0] + v2[0] * s;
            dest[1] = v1[1] + v2[1] * s;
            dest[2] = v1[2] + v2[2] * s;
        }

        public static float[] VSub(float v1x, float v1y, float v1z, float v2x, float v2y, float v2z)
        {
            return new float[]
            {
                v1x - v2x, v1y - v2y, v1z - v2z
            };
        }

        public static float[] VAdd(float v1x, float v1y, float v1z, float v2x, float v2y, float v2z)
        {
            return new float[]
            {
                v1x + v2x, v1y + v2y, v1z + v2z 
            };
        }

        public static float VDistSqr(float v1x, float v1y, float v1z, float v2x, float v2y, float v2z)
        {
            float dx = v2x - v1x;
            float dy = v2y - v1y;
            float dz = v2z - v1z;
            return dx*dx + dy*dy + dz*dz;
        }

        public static float VDistSqr(float v1x, float v1y, float v1z, Vector3 v2)
        {
            return Vector3.DistanceSquared(new Vector3(v1x, v1y, v1z), v2);
        }

        public static bool OverlapQuantBounds(Vector3i amin, Vector3i amax, Vector3i bmin, Vector3i bmax)
        {
            bool overlap = true;
            overlap = (amin.X > bmax.X || amax.X < bmin.X) ? false : overlap;
            overlap = (amin.Y > bmax.Y || amax.Y < bmin.Y) ? false : overlap;
            overlap = (amin.Z > bmax.Z || amax.Z < bmin.Z) ? false : overlap;
            return overlap;
        }

        public static void VMin(ref Vector3 mn, float vx, float vy, float vz)
        {
            mn = Vector3.Min(mn, new Vector3(vx, vy, vz));
        }

        public static void VMax(ref float[] mn, float vx, float vy, float vz)
        {
            mn[0] = Math.Max(mn[0], vx);
            mn[1] = Math.Max(mn[1], vx);
            mn[2] = Math.Max(mn[2], vx);
        }

        public static bool OverlapBounds(Vector3 amin, Vector3 amax, Vector3 bmin, Vector3 bmax)
        {
            bool overlap = true;
            overlap = (amin.X > bmax.X || amax.X < bmin.X) ? false : overlap;
            overlap = (amin.Y > bmax.Y || amax.Y < bmin.Y) ? false : overlap;
            overlap = (amin.Z > bmax.Z || amax.Z < bmin.Z) ? false : overlap;
            return overlap;

        }

        public static long HashRef(long a)
        {
            a += ~(a << 15);
            a ^= (a >> 10);
            a += (a << 3);
            a ^= (a >> 6);
            a += ~(a << 11);
            a ^= (a >> 16);
            return a;
        }

        public static float TriArea2D(float[] a, float[] b, float[] c)
        {
            float abx = b[0] - a[0];
            float abz = b[2] - a[2];
            float acx = c[0] - a[0];
            float acz = c[2] - a[2];
            return acx*abz - abx*acz;
        }

        public static float TriArea2D(Vector3 a, Vector3 b, Vector3 c)
        {
            var ab = b - a;
            var ac = c - a;
            return ac.X * ab.Z - ab.X * ac.Z;
        }

        public static void RandomPointInConvexPoly(float[] pts, int npts, ref float[] areas, float s, float t, ref Vector3 outPt)
        {
            float areasum = 0.0f;
            float[] va = new float[3], vb = new float[3], vc = new float[3];
            for (int i = 2; i < npts; i++)
            {
                Array.Copy(pts, 0, va, 0, 3);
                Array.Copy(pts, (i-1)*3, vb, 0, 3);
                Array.Copy(pts, i*3, vc, 0, 3);
                areas[i] = TriArea2D(va, vb, vc);
                areasum += Math.Max(0.001f, areas[i]);
            }

            float thr = s*areasum;
            float acc = 0.0f;
            float u = 0.0f;
            int tri = 0;
            for (int i = 2; i < npts; i++)
            {
                float dacc = areas[i];
                if (thr >= acc && thr < (acc + dacc))
                {
                    u = (thr - acc)/dacc;
                    tri = i;
                    break;
                }
                acc += dacc;
            }

            float v = (float)Math.Sqrt(t);

            float a = 1 - v;
            float b = (1 - u)*v;
            float c = u*v;
            int pa = 0;
            int pb = (tri - 1)*3;
            int pc = tri*3;

            outPt = a * V3(pts, pa) + b * V3(pts, pb) + c * V3(pts, pc);
        }

        public static bool VEqual(float p0x, float p0y, float p0z, float p1x, float p1y, float p1z)
        {
            const float thr = ((1.0f/16384.0f)*(1.0f/16384.0f));
            float d = VDistSqr(p0x, p0y, p0z, p1x, p1y, p1z);
            return d < thr;
        }

        public static bool VEqual(Vector3 p0, Vector3 p1)
        {
            const float thr = ((1.0f / 16384.0f) * (1.0f / 16384.0f));
            float d = Vector3.DistanceSquared(p0, p1);
            return d < thr;
        }

        public static bool IntersectSegSeg2D(Vector3 ap, Vector3 aq, Vector3 bp, Vector3 bq, ref float s, ref float t)
        {
            var u = Vector3.Subtract(aq, ap);
            var v = Vector3.Subtract(bq, bp);
            var w = Vector3.Subtract(ap, bp);
            float d = VPerpXZ(u, v);
            if (Math.Abs(d) < 1e-6f) return false;
            s = VPerpXZ(v, w)/d;
            t = VPerpXZ(u, w)/d;
            return true;
        }

        private static float VPerpXZ(Vector3 a, Vector3 b)
        {
            return a.X*b.Z - a.Z*b.X;
        }

        public static bool PointInPolygon(Vector3 pt, float[] verts, int nverts)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = nverts-1; i < nverts; j=i++)
            {
                int vi = i*3;
                int vj = j*3;
                if (((verts[vi + 2] > pt.Z) != (verts[vj + 2] > pt.Z)) &&
                    (pt.X < (verts[vj + 0] - verts[vi + 0])*(pt.Z - verts[vi + 2])/(verts[vj + 2] - verts[vi + 2]) + verts[vi + 0]))
                    c = !c;
            }
            return c;
        }

        public static bool IntersectSegmentPoly2D(Vector3 p0, Vector3 p1, float[] verts, int nverts, out float tmin, out float tmax, out int segMin, out int segMax)
        {
            const float EPS = 0.00000001f;

            tmin = 0;
            tmax = 1;
            segMin = -1;
            segMax = -1;

            var dir = p1 - p0; ;

            for (int i = 0, j = nverts-1; i < nverts; j=i++)
            {
                var edge = V3(verts, i * 3) - V3(verts, j * 3);
                var diff = p0 - V3(verts, j * 3);
                float n = VPerp2D(edge, diff);
                float d = VPerp2D(dir, edge);
                if (Math.Abs(d) < EPS)
                {
                    if (n < 0)
                        return false;
                    else
                    {
                        continue;
                    }
                }
                float t = n/d;
                if (d < 0)
                {
                    if (t > tmin)
                    {
                        tmin = t;
                        segMin = j;
                        if (tmin > tmax)
                            return false;
                    }
                }
                else
                {
                    if (t < tmax)
                    {
                        tmax = t;
                        segMax = j;
                        if (tmax < tmin)
                            return false;
                    }
                }
            }
            return true;
        }

        public static float VPerp2D(float[] u, float[] v)
        {
            return u[2]*v[0] - u[0]*v[2];
        }

        public static float VPerp2D(Vector3 u, Vector3 v)
        {
            return u.Z*v.X - u.X*v.Z;
        }

        public static void VNormalize(ref float[] v)
        {
            float d = 1.0f/(float)Math.Sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
            v[0] *= d;
            v[1] *= d;
            v[2] *= d;
        }

        public static float[] VScale(float vx, float vy, float vz, float t)
        {
            float[] dest = {vx*t, vy*t, vz*t};
            return dest;
        }

        public static void Rotate2D(ref float[] dest, int idx, float[] direction, float angle)
        {
            dest[idx] = direction[0] * MathF.Cos(angle) - direction[2] * MathF.Sin(angle);
            dest[idx + 1] = direction[1];
            dest[idx + 2] = direction[0] * MathF.Sin(angle) + direction[2] * MathF.Cos(angle);
        }

        public static bool OverlapPolyPoly2D(float[] polya, int npolya, float[] polyb, int npolyb)
        {
            const float eps = 1e-4f;

            for (int i = 0, j = npolya-1; i < npolya; j=i++)
            {
                int va = j*3;
                int vb = i*3;
                var n = new Vector3(polya[vb + 2] - polya[va + 2], 0, -(polya[vb + 0] - polya[va + 0]));
                float amin, amax, bmin, bmax;
                ProjectPoly(n, polya, npolya, out amin, out amax);
                ProjectPoly(n, polyb, npolyb, out bmin, out bmax);
                if (!OverlapRange(amin, amax, bmin, bmax, eps))
                    return false;
            }
            for (int i = 0, j = npolya - 1; i < npolyb; j = i++)
            {
                int va = j * 3;
                int vb = i * 3;
                var n = new Vector3(polyb[vb + 2] - polyb[va + 2], 0, -(polyb[vb + 0] - polyb[va + 0]));
                float amin, amax, bmin, bmax;
                ProjectPoly(n, polya, npolya, out amin, out amax);
                ProjectPoly(n, polyb, npolyb, out bmin, out bmax);
                if (!OverlapRange(amin, amax, bmin, bmax, eps))
                    return false;
            }
            return true;
        }

        private static bool OverlapRange(float amin, float amax, float bmin, float bmax, float eps)
        {
            return ((amin + eps) > bmax || (amax - eps) < bmin) ? false : true;
        }

        private static void ProjectPoly(Vector3 axis, float[] poly, int npoly, out float rmin, out float rmax)
        {
            var temp = new Vector3();

            CopyTo(poly, 0, ref temp);
            rmin = rmax = VDot2D(axis, temp);
            for (int i = 1; i < npoly; i++)
            {
                CopyTo(poly, i*3, ref temp);
                float d = VDot2D(axis, temp);
                rmin = Math.Min(rmin, d);
                rmax = Math.Max(rmax, d);
            }
        }

        public static void VSet(ref float[] dest, float x, float y, float z)
        {
            dest[0] = x;
            dest[1] = y;
            dest[2] = z;
        }

        public static void VSet(ref Vector3 dest, float x, float y, float z)
        {
            dest.X = x;
            dest.Y = y;
            dest.Z = z;
        }

        public static void VCopy(ref float[] dest, float[] a)
        {
            dest[0] = a[0];
            dest[1] = a[1];
            dest[2] = a[2];
        }

        public static void VCopy(ref float[] dest, Vector3 a)
        {
            CopyTo(a, ref dest, 0);
        }

        public static float VDist2D(float[] v1, float[] v2)
        {
            float dx = v2[0] - v1[0];
            float dz = v2[2] - v1[2];
            return (float)Math.Sqrt(dx*dx + dz*dz);
        }

        public static float VDist2D(Vector3 v1, Vector3 v2)
        {
            return Vector2.Distance(V2(v2), V2(v1));
        }

        public static float VDist2DSqr(float v1x, float v1y, float v1z, float v2x, float v2y, float v2z)
        {
            float dx = v2x - v1x;
            float dz = v2z - v1z;
            return dx*dx + dz*dz;
        }

        public static float VLen(float[] v)
        {
            return (float) Math.Sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
        }

        public static float VLenSqr(float[] v)
        {
            return v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
        }

        public static unsafe void CopyTo(float[] source, int sourceIdx, ref Vector3 dest)
        {
            fixed (float* p = &source[sourceIdx])
                Unsafe.Copy(ref dest, p);
        }

        public static unsafe void CopyTo(Vector3 source, ref float[] dest, int destIdx)
        {
            fixed (float* p = &dest[destIdx])
                Unsafe.Copy(p, ref source);
        }

        public static Vector3 V3(float[] source) => V3(source, 0);

        public static Vector3 V3(float[] source, int sourceIdx)
        {
            return new Vector3(source[sourceIdx],
                source[sourceIdx + 1],
                source[sourceIdx + 2]);
        }

        public static float[] F3(Vector3 v)
            => new[] { v.X, v.Y, v.Z };

        /// <summary>
        /// a vector2, using v.Z as the new Y
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector2 V2(Vector3 v)
            => new Vector2(v.X, v.Z);
    }
}