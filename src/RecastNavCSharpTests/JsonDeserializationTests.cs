using System.Text.Json;

namespace RecastNavCSharpTests
{
    [TestClass]
    public class JsonDeserializationTests
    {
        [TestMethod]
        [DeploymentItem("data_json/ck_spa.navmesh")]
        public void TestDeserializedFileValues()
        {
            var ser = DeserializeFile("data_json/ck_spa.navmesh");
            
            Assert.IsNotNull(ser);
            Assert.IsTrue(ser.NavMeshBuilders.Length > 0);
            Assert.IsTrue(ser.NavMeshBuilders[0].NavPolys.Length > 0);
            Assert.AreEqual(1, ser.NavMeshBuilders[0].NavPolys[0].Verts[0], "Unexpected first poly virt");
        }


        private NavMeshSerializer DeserializeFile(string filePath)
        {
            using var f = File.OpenRead(filePath);
            var options = new JsonSerializerOptions
            {
                AllowTrailingCommas = true,
                PropertyNameCaseInsensitive = true,
                ReadCommentHandling = JsonCommentHandling.Skip
            };
            return JsonSerializer.Deserialize<NavMeshSerializer>(f, options);
        }
    }
}